# Author: Jeremiasz Hauck
# 2019-08-28

import meta2
import re

LD = 1
LDL = 2
ST = 3
ADD = 4
SUB = 5
MLT = 6
EQU = 7
B = 8
BFP = 9
BTP = 10
EDT = 11
PNT = 12
SP = 13
BLK = 14
END = 15

opname_to_code = {
  'LD': 1,
  'LDL': 2,
  'ST': 3,
  'ADD': 4,
  'SUB': 5,
  'MLT': 6,
  'EQU': 7,
  'B': 8,
  'BFP': 9,
  'BTP': 10,
  'EDT': 11,
  'PNT': 12,
  'SP': 13,
  'BLK': 14,
  'END': 15}

code_to_opname = {v: k for k, v in opname_to_code.items()}

num_args = {
  'LD': 1,
  'LDL': 1,
  'ST': 1,
  'ADD': 0,
  'SUB': 0,
  'MLT': 0,
  'EQU': 0,
  'B': 1,
  'BFP': 1,
  'BTP': 1,
  'EDT': 1,
  'PNT': 0,
  'SP': 1,
  'BLK': 1,
  'END': 0}

valgol1 = """
.SYNTAX PROGRAM

PRIMARY = .ID .OUT('LD ' *) /
.NUMBER .OUT('LDL' *) /
'(' EXP1 ')';

TERM = PRIMARY $('*' PRIMARY .OUT('MLT'));

EXP1 = TERM $('+' TERM .OUT('ADD') /
'-' TERM .OUT('SUB'));

EXP = EXP1 ('==' EXP1 .OUT('EQU') / .EMPTY);

ASSIGNST = EXP '=' .ID .OUT('ST ' *);

UNTILST = '.until' .LABEL *1 EXP '.do' .OUT('BTP' *2)
ST .OUT('B  ' *1) .LABEL *2;

CONDITIONALST = '.if' EXP '.then' .OUT('BFP' *1)
ST '.else' .OUT('B  ' *2) .LABEL *1
ST .LABEL *2;

IOST = '.edit' '(' EXP ',' .STRING
.OUT('EDT' *) ')' /
'.print' .OUT('PNT');

IDSEQ1 = .ID .LABEL * .OUT('BLK' '1');

IDSEQ = IDSEQ1 $(',' IDSEQ1);

DEC = '.real' .OUT('B  ' *1) IDSEQ .LABEL *1;

BLOCK = '.begin' (DEC ';' / .EMPTY)
ST $(';' ST) '.end';

ST = IOST / ASSIGNST / UNTILST /
CONDITIONALST / BLOCK;

PROGRAM = BLOCK .OUT('END');

.END
"""

def print_trace(trace, ip, stack_len, code, code_to_label, *argv):
  if trace:
    depth = 2 * ((stack_len // 3) - 1)
    print(depth * ' ', end='')
    opname = code_to_opname[code[ip]] 
    print(opname, end=' ')
    if num_args[opname] == 1:
      if opname == 'EDT':
        print("'", end='')
        print(code[ip + 1], end="' ")
      if opname == 'LDL' or opname == 'SP' or opname == 'BLK':
        print(code[ip + 1])
      else: # branch instructions
        print(code_to_label[code[ip + 1]], end=' ')
    for arg in argv:
      print(arg, end=' ')
    print()

def valgol1_machine(code, trace=False):
  code, labels, mem_size = label_to_address(code)
  code_to_label = {v: k for k, v in labels.items()}
  ip = 0
  stack = []
  mem = mem_size * [0]
  error = None
  
  while ip < len(code):
    op = code[ip]
    ip += 1
    if op == LD:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      stack.append(mem[code[ip]])
      ip += 1
    elif op == LDL:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      stack.append(code[ip])
      ip += 1
    elif op == ST:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      addr = code[ip]
      ip += 1
      top = stack.pop()
      mem[addr] = top
    elif op == ADD:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      stack.append(stack.pop() + stack.pop())
    elif op == SUB:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      arg1 = stack.pop()
      arg2 = stack.pop()
      stack.append(arg2 - arg1)
    elif op == MLT:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      stack.append(stack.pop() * stack.pop())
    elif op == EQU:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      if stack.pop() == stack.pop():
        stack.append(1)
      else:
        stack.append(0)
    elif op == B:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      ip = code[ip]
    elif op == BFP:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      if stack.pop() == 0:
        ip = code[ip]
      else:
        ip += 1
    elif op == BTP:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      if stack.pop() != 0:
        ip = code[ip]
      else:
        ip += 1
    elif op == EDT:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      n = int(stack.pop())
      to_print = code[ip]
      ip += 1
      if n == 0:
        print(to_print, end='')
      else:
        print(max(0, (n - 2)) * ' ', to_print, end='')
    elif op == PNT:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      print()
    elif op == SP:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      n = code[ip]
      ip += 1
      print(n * ' ', end='')
    elif op == BLK:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      #mem.append(None)
    elif op == END:
      print_trace(trace, ip - 1, len(stack), code, code_to_label)
      break
    else:
      error = 'error - Unknown code : {}'.format(op)
      break
  
  if error == None:
    #print('\nDone')
    print()
  else:
    print(error)

    
# a test program writen in Valgol I
#   that draws a parabola with '*' sign
test_code = """
.begin
.real x, y; 0 = x; 0 = y;
.until y == 45 .do .begin
  .edit(x*x*10+1, '*');
  .print;
  x + 0.1 = x;
  y + 1 = y
  .end
.end
"""

def is_number(x):
  try:
    float(x)
    return True
  except ValueError:
    return False

def gencode_to_code(gencode, op_to_code):
  gencode = meta2.splitter(gencode)
  #print(gencode)
  code = []
  for x in gencode:
    if x in op_to_code:
      code.append(op_to_code[x])
    elif x.startswith("'") and x.endswith("'"):
      code.append(x[1:-1])
    elif x.isdigit():
      code.append(int(x))
    elif is_number(x):
      code.append(float(x))
    else:
      code.append(x)
  return code

def label_to_address(code):
  labels = {}
  code_addr = 0
  mem_addr = 0
  k = 0
  while k < len(code):
    x = code[k]
    k += 1
    if isinstance(x, str):
      if x.startswith(':'):
        if k < len(code) and code[k] == BLK:
          labels[x[1:]] = mem_addr
          mem_addr += code[k + 1]
        else:
          labels[x[1:]] = code_addr
      else:
        code_addr += 1
    else:
      code_addr += 1
  new_code = []
  for x in code:
    if isinstance(x, str):
      if not x.startswith(':'):
        if x in labels:
          new_code.append(labels[x])
        else:
          new_code.append(x)
    else:
      new_code.append(x)
  mem_size = mem_addr + 1
  return new_code, labels, mem_size

def test():
  valgol1_compiler_prog, valgol1_compiler_string = meta2.compile_compiler(valgol1)
  meta2.print_code(valgol1_compiler_string)
  print()
  gen_code = meta2.compile_with_meta2(valgol1_compiler_prog, test_code, trace=True)
  meta2.print_code(gen_code, num_args)

  code = gencode_to_code(gen_code, opname_to_code)
  print(code)

def test2():
  valgol1_compiler_prog, valgol1_compiler_string = meta2.compile_compiler(valgol1)
  gen_code = meta2.compile_with_meta2(valgol1_compiler_prog, test_code, trace=True)
  code = gencode_to_code(gen_code, opname_to_code)
  valgol1_machine(code)
  
if __name__ == '__main__':
  #test()
  test2()

