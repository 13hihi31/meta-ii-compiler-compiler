# META II Compiler Compiler

Implementation of core ideas expressed in the paper: "META II A SYNTAX-ORIENTED COMPILER WRITING LANGUAGE" by D. V. Schorre, UCLA Computing Facility.

META II compiles compilers directly from syntax equations. Since META II is defined in syntax equations as well, it can compile it self.
In the attached image below one can see the possible usage of the META II compiler. The META II machine is a virtual stack machine with a small set of machine instructions.

The best way to understand the META II compiler is by generating the META II compiler machine code directly from the syntax equations using pen and paper as tools. Compiled machine code of META II can be found in the file meta2.py for comparison.

![](Meta2_2.svg "META II typical usage flow")